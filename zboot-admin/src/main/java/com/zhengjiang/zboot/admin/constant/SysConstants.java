package com.zhengjiang.zboot.admin.constant;

/**
 * 常量管理
 * @author Louis
 * @date Jan 13, 2019
 */
public interface SysConstants {

	/**
	 * 系统管理员用户名
	 */
	String ADMIN = "admin";

	/**
	 * 菜单类型-目录
	 */
	Integer MENU_TYPE_DIRECTORY = 0;

	/**
	 * 菜单类型-URL
	 */
	Integer MENU_TYPE_URL = 1 ;

	/**
	 * 菜单类型-按钮
	 */
	Integer MENU_TYPE_BUTTON = 2 ;


}
