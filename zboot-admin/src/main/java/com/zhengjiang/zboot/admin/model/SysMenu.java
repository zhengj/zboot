package com.zhengjiang.zboot.admin.model;

import lombok.Data;

import java.util.List;

@Data
public class SysMenu extends BaseModel {

    private Long parentId;
    private String name;
    private String url;
    private String perms;
    //菜单类型 0-目录 1-菜单 2-按钮
    private Integer type;
    private String icon;
    private Integer orderNum;
    private Byte delFlag;
    // 非数据库字段
    private String parentName;
    // 非数据库字段
    private Integer level;
    // 非数据库字段
    private List<SysMenu> children;

}
