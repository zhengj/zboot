package com.zhengjiang.zboot.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.zhengjiang.zboot"})
public class ZbootAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZbootAdminApplication.class, args);
	}
}

