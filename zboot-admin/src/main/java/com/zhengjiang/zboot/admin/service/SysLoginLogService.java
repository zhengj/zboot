package com.zhengjiang.zboot.admin.service;

import com.zhengjiang.zboot.admin.model.SysLoginLog;
import com.zhengjiang.zboot.core.service.CurdService;

/**
 * 登录日志
 * @author Louis
 * @date Jan 13, 2019
 */
public interface SysLoginLogService extends CurdService<SysLoginLog> {

}
