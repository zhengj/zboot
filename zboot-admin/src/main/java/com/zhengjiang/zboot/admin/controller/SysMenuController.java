package com.zhengjiang.zboot.admin.controller;

import java.util.List;

import com.google.common.collect.Lists;
import com.zhengjiang.zboot.admin.constant.SysConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zhengjiang.zboot.admin.model.SysMenu;
import com.zhengjiang.zboot.admin.service.SysMenuService;
import com.zhengjiang.zboot.core.http.HttpResult;

/**
 * 菜单控制器
 * @author Louis
 * @date Jan 13, 2019
 */
@RestController
@RequestMapping("menu")
public class SysMenuController {

	@Autowired
	private SysMenuService sysMenuService;

	@PreAuthorize("hasAuthority('sys:menu:add') AND hasAuthority('sys:menu:edit')")
	@PostMapping(value="/save")
	public HttpResult save(@RequestBody SysMenu record) {
		return HttpResult.ok(sysMenuService.save(record));
	}

	@PreAuthorize("hasAuthority('sys:menu:delete')")
	@PostMapping(value="/delete")
	public HttpResult delete(@RequestBody List<SysMenu> records) {
		return HttpResult.ok(sysMenuService.delete(records));
	}

	@PreAuthorize("hasAuthority('sys:menu:view')")
	@GetMapping(value="/findNavTree")
	public HttpResult findNavTree(@RequestParam String userName) {
		List<Integer> menuTypes = Lists.newArrayList(SysConstants.MENU_TYPE_DIRECTORY,SysConstants.MENU_TYPE_URL);
		return HttpResult.ok(sysMenuService.findTree(userName, menuTypes));
	}

	@PreAuthorize("hasAuthority('sys:menu:view')")
	@GetMapping(value="/findMenuTree")
	public HttpResult findMenuTree() {
		List<Integer> menuTypes = Lists.newArrayList(SysConstants.MENU_TYPE_DIRECTORY,SysConstants.MENU_TYPE_URL,SysConstants.MENU_TYPE_BUTTON);
		return HttpResult.ok(sysMenuService.findTree(null, menuTypes));
	}
}
