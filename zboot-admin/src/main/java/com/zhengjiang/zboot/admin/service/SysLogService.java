package com.zhengjiang.zboot.admin.service;

import com.zhengjiang.zboot.admin.model.SysLog;
import com.zhengjiang.zboot.core.service.CurdService;

/**
 * 操作日志
 * @author Louis
 * @date Jan 13, 2019
 */
public interface SysLogService extends CurdService<SysLog> {

}
