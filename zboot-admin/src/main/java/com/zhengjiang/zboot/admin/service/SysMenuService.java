package com.zhengjiang.zboot.admin.service;

import java.util.List;

import com.zhengjiang.zboot.admin.model.SysMenu;
import com.zhengjiang.zboot.core.service.CurdService;

/**
 * 菜单管理
 * @author Louis
 * @date Jan 13, 2019
 */
public interface SysMenuService extends CurdService<SysMenu> {

	/**
	 * 查询菜单树,用户ID和用户名为空则查询全部
	 * @param userName 用户名
	 * @param menuTypes  menuTypes 获取菜单类型集合
	 * @return
	 */
	List<SysMenu> findTree(String userName, List<Integer> menuTypes);

	/**
	 * 根据用户名查找菜单列表
	 * @param userName
	 * @return
	 */
	List<SysMenu> findByUser(String userName);
}
