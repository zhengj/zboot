package com.zhengjiang.zboot.admin.service;

import java.util.List;

import com.zhengjiang.zboot.admin.model.SysConfig;
import com.zhengjiang.zboot.core.service.CurdService;

/**
 * 系统配置管理
 * @author Louis
 * @date Jan 13, 2019
 */
public interface SysConfigService extends CurdService<SysConfig> {

	/**
	 * 根据名称查询
	 * @param lable
	 * @return
	 */
	List<SysConfig> findByLable(String lable);
}
