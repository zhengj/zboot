package com.zhengjiang.zboot.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ZbootServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZbootServiceApplication.class,args);
    }
}
