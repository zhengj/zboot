package com.zhengjiang.zboot.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @ClassName ZbootGatewayApplication
 * @Description
 * @Author zhengjiang(99514925 @ qq.com)
 * @Date 2020/4/16 9:51
 * @Version V1.0
 **/
@EnableZuulProxy
@EnableAutoConfiguration
public class ZbootGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZbootGatewayApplication.class,args);
    }
}
