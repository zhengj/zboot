package com.zhengjiang.zboot.backup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动器
 */
@SpringBootApplication(scanBasePackages={"com.zhengjiang.zboot"})
public class ZbootBackupApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZbootBackupApplication.class, args);
	}
}
