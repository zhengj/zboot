package com.zhengjiang.zboot.monitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

/**
 * 启动器
 */
@EnableAdminServer
@SpringBootApplication
public class ZbootMonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZbootMonitorApplication.class, args);
	}
}
