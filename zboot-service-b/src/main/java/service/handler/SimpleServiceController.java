package service.handler;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/simple")
public class SimpleServiceController {

    @PostMapping("/hello")
    public String hello(String name){
        return "hello b " + name;
    }

}
