package com.zhengjiang.zboot.feign.service;
import com.zhengjiang.zboot.feign.http.HttpResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "zboot-admin")
public interface FeignProviderClient {

    @GetMapping(value="/user/findByName")
    HttpResult findByUserName(@RequestParam String name) ;
}
