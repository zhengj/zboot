package com.zhengjiang.zboot.feign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @ClassName MangoFeignApplication
 * @Description
 * @Author zhengjiang(99514925 @ qq.com)
 * @Date 2020/4/15 22:56
 * @Version V1.0
 **/
@SpringBootApplication
@EnableFeignClients
public class MangoFeignApplication {
    public static void main(String[] args) {
        SpringApplication.run(MangoFeignApplication.class,args);
    }
}
