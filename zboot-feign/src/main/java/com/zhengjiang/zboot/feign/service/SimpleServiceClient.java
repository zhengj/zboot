package com.zhengjiang.zboot.feign.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "zboot-service",fallback = SimpleServiceHystrix.class)
public interface SimpleServiceClient {

    @PostMapping(value = "/simple/hello")
    String hello(@RequestParam  String name);

    @GetMapping(value = "/simple/index")
    String index();

}
