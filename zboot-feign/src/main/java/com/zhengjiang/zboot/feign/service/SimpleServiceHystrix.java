package com.zhengjiang.zboot.feign.service;

import org.springframework.stereotype.Component;

@Component
public class SimpleServiceHystrix implements SimpleServiceClient {

    @Override
    public String hello(String name) {
        return "SimpleService call failed ! " + name;
    }

    @Override
    public String index() {
        return "SimpleService index failed!";
    }
}
