package com.zhengjiang.zboot.feign.controller;

import com.zhengjiang.zboot.feign.http.HttpResult;
import com.zhengjiang.zboot.feign.service.FeignProviderClient;
import com.zhengjiang.zboot.feign.service.SimpleServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName FeignHandler
 * @Description
 * @Author zhengjiang(99514925 @ qq.com)
 * @Date 2020/4/15 23:08
 * @Version V1.0
 **/
@RestController
@RequestMapping("/feign")
public class FeignHandler {

   /* @Autowired
    private FeignProviderClient feignProviderClient;
*/
    @Resource
    private SimpleServiceClient simpleServiceClient;


   /* @GetMapping(value="/findUserByName")
    public HttpResult findByUserName(@RequestParam String name, HttpServletRequest request){
        return feignProviderClient.findByUserName(name);
    }*/

    @PostMapping(value = "/findSimpleName")
    public String findSimpleName(@RequestParam String name){
        return simpleServiceClient.hello(name);
    }

    @GetMapping(value = "/index")
    public String index(){
        return simpleServiceClient.index();
    }

}
