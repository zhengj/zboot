package com.zhengjiang.zboot.ereka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @ClassName MangoErekaApplication
 * @Description
 * @Author zhengjiang(99514925 @ qq.com)
 * @Date 2020/4/15 22:14
 * @Version V1.0
 **/
@SpringBootApplication
@EnableEurekaServer
public class ZbootErekaApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZbootErekaApplication.class,args);
    }
}
